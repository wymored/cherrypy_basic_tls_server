#!/usr/bin/env python3

import cherrypy
import os
from cherrypy._cpserver import Server

cherrypy.config.update({'server.socket_port': 80})





class app(object):

    @cherrypy.expose
    def default(self,*args,**kwargs):
        servername = '127.0.0.1'

        return """
        <!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="refresh" content="1;url={0}">
        <script type="text/javascript">
            window.location.href = "{0}"
        </script>
        <title>Page Redirection</title>
    </head>
    <body>
        <!-- Note: don't tell people to `click` the link, just tell them that it is a link. -->
        If you are not redirected automatically, follow the <a href='{0}'>link </a>
    </body>
</html>
        """.format('https://'+cherrypy.url(qs=cherrypy.request.query_string).replace("http:",""))








if __name__ == '__main__':
    cherrypy.quickstart(app())