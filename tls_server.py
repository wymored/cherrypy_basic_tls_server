#!/usr/bin/env python3

import cherrypy
import os
from cherrypy._cpserver import Server

cherrypy.config.update({'server.socket_port': 443})


cherrypy.server.ssl_module = 'builtin'

path = os.path.dirname(os.path.abspath(__file__))
print(path)

cherrypy.server.ssl_certificate = "{}/ssl/cert.pem".format(path)
cherrypy.server.ssl_private_key = "{}/ssl/privkey.pem".format(path)
#cherrypy.server.ssl_certificate_chain = "certchain.perm"




class app(object):

    @cherrypy.expose
    def index(self):
        return "Hello world!b"

    @cherrypy.expose
    def generate(self):
        import random,string
        return ''.join(random.sample(string.hexdigits, 8))






if __name__ == '__main__':
    cherrypy.quickstart(app())